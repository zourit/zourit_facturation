require "test_helper"

class FormsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form = forms(:one)
  end

  test "should get index" do
    get forms_url
    assert_response :success
  end

  test "should get new" do
    get new_form_url
    assert_response :success
  end

  test "should create form" do
    assert_difference("Form.count") do
      post forms_url, params: { form: { address: @form.address, comments: @form.comments, community_price: @form.community_price, dedicated_cloud: @form.dedicated_cloud, domain: @form.domain, email: @form.email, first_account: @form.first_account, first_name: @form.first_name, garradin: @form.garradin, last_name: @form.last_name, name: @form.name, phone: @form.phone, quota: @form.quota, start_date: @form.start_date, sympa: @form.sympa, town: @form.town, zip_code: @form.zip_code } }
    end

    assert_redirected_to form_url(Form.last)
  end

  test "should show form" do
    get form_url(@form)
    assert_response :success
  end

  test "should get edit" do
    get edit_form_url(@form)
    assert_response :success
  end

  test "should update form" do
    patch form_url(@form), params: { form: { address: @form.address, comments: @form.comments, community_price: @form.community_price, dedicated_cloud: @form.dedicated_cloud, domain: @form.domain, email: @form.email, first_account: @form.first_account, first_name: @form.first_name, garradin: @form.garradin, last_name: @form.last_name, name: @form.name, phone: @form.phone, quota: @form.quota, start_date: @form.start_date, sympa: @form.sympa, town: @form.town, zip_code: @form.zip_code } }
    assert_redirected_to form_url(@form)
  end

  test "should destroy form" do
    assert_difference("Form.count", -1) do
      delete form_url(@form)
    end

    assert_redirected_to forms_url
  end
end
