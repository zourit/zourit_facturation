require "application_system_test_case"

class FormsTest < ApplicationSystemTestCase
  setup do
    @form = forms(:one)
  end

  test "visiting the index" do
    visit forms_url
    assert_selector "h1", text: "Forms"
  end

  test "should create form" do
    visit forms_url
    click_on "New form"

    fill_in "Address", with: @form.address
    fill_in "Comments", with: @form.comments
    check "Community price" if @form.community_price
    check "Dedicated cloud" if @form.dedicated_cloud
    fill_in "Domain", with: @form.domain
    fill_in "Email", with: @form.email
    fill_in "First account", with: @form.first_account
    fill_in "First name", with: @form.first_name
    check "Garradin" if @form.garradin
    fill_in "Last name", with: @form.last_name
    fill_in "Name", with: @form.name
    fill_in "Phone", with: @form.phone
    fill_in "Quota", with: @form.quota
    fill_in "Start date", with: @form.start_date
    check "Sympa" if @form.sympa
    fill_in "Town", with: @form.town
    fill_in "Zip code", with: @form.zip_code
    click_on "Create Form"

    assert_text "Form was successfully created"
    click_on "Back"
  end

  test "should update Form" do
    visit form_url(@form)
    click_on "Edit this form", match: :first

    fill_in "Address", with: @form.address
    fill_in "Comments", with: @form.comments
    check "Community price" if @form.community_price
    check "Dedicated cloud" if @form.dedicated_cloud
    fill_in "Domain", with: @form.domain
    fill_in "Email", with: @form.email
    fill_in "First account", with: @form.first_account
    fill_in "First name", with: @form.first_name
    check "Garradin" if @form.garradin
    fill_in "Last name", with: @form.last_name
    fill_in "Name", with: @form.name
    fill_in "Phone", with: @form.phone
    fill_in "Quota", with: @form.quota
    fill_in "Start date", with: @form.start_date
    check "Sympa" if @form.sympa
    fill_in "Town", with: @form.town
    fill_in "Zip code", with: @form.zip_code
    click_on "Update Form"

    assert_text "Form was successfully updated"
    click_on "Back"
  end

  test "should destroy Form" do
    visit form_url(@form)
    click_on "Destroy this form", match: :first

    assert_text "Form was successfully destroyed"
  end
end
