Rails.application.routes.draw do
  resources :invoices
  resources :organizations do
    resources :histories
  end
  resources :services
  resources :forms
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get '/structures' => "forms#structures"
  get '/structures/:id' => "forms#structure"
end
