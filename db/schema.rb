# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_08_122232) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "forms", force: :cascade do |t|
    t.date "start_date"
    t.string "name"
    t.string "address"
    t.string "zip_code"
    t.string "town"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.string "domain"
    t.string "first_account"
    t.integer "quota"
    t.boolean "garradin"
    t.boolean "sympa"
    t.boolean "dedicated_cloud"
    t.boolean "community_price"
    t.text "comments"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "annual_price"
    t.boolean "cgu", default: true
  end

  create_table "histories", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.integer "quota"
    t.string "services_ids"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "date"
    t.index ["organization_id"], name: "index_histories_on_organization_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.bigint "history_id", null: false
    t.float "price"
    t.integer "days"
    t.boolean "invoiced"
    t.boolean "invoiced_provider"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["history_id"], name: "index_invoices_on_history_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "zip_code"
    t.string "town"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.string "domain"
    t.string "first_account"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "title"
    t.string "icon"
    t.text "text"
    t.float "annual_price"
    t.boolean "mandatory", default: true
    t.string "program"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "histories", "organizations"
  add_foreign_key "invoices", "histories"
end
