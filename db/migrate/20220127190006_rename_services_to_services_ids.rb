class RenameServicesToServicesIds < ActiveRecord::Migration[7.0]
  def change
    rename_column :histories, :services, :services_ids
  end
end
