class CreateHistories < ActiveRecord::Migration[7.0]
  def change
    create_table :histories do |t|
      t.references :organization, null: false, foreign_key: true
      t.integer :quota
      t.string :services
      t.text :comment

      t.timestamps
    end
  end
end
