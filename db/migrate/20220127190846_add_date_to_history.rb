class AddDateToHistory < ActiveRecord::Migration[7.0]
  def change
    add_column :histories, :date, :date
  end
end
