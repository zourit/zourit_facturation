class AddCguToForms < ActiveRecord::Migration[7.0]
  def change
    add_column :forms, :cgu, :boolean, default: true
  end
end
