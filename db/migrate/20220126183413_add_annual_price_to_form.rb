class AddAnnualPriceToForm < ActiveRecord::Migration[7.0]
  def change
    add_column :forms, :annual_price, :float
  end
end
