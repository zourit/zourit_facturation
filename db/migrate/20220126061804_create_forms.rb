class CreateForms < ActiveRecord::Migration[7.0]
  def change
    create_table :forms do |t|
      t.date :start_date
      t.string :name
      t.string :address
      t.string :zip_code
      t.string :town
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :domain
      t.string :first_account
      t.integer :quota, default: 50
      t.boolean :garradin, default: false
      t.boolean :sympa, default: false
      t.boolean :dedicated_cloud, default: false
      t.boolean :community_price, default: true
      t.text :comments

      t.timestamps
    end
  end
end
