class CreateServices < ActiveRecord::Migration[7.0]
  def change
    create_table :services do |t|
      t.string :title
      t.string :icon
      t.text :text
      t.float :annual_price
      t.boolean :mandatory, default:true
      t.string :program

      t.timestamps
    end

    services = [
      {
        icon: 'envelope',
        title: 'Courriel, agendas, contacts',
        text: 'Consultez vos mails, agendas partagés, contacts. Prenez rendez-vous ou tchattez en direct grâce à Zimbra.',
        program: 'Zimbra'
      },
      {
        icon: 'cloud',
        title: 'Fichiers dans le cloud',
        text: 'Grace au service Nextcloud, stockez vos fichiers en ligne afin de les retrouver à tout moment. Partagez-les simplement avec vos collègues.',
        program: 'Nextcloud'
      },
      {
        icon: 'pencil',
        title: 'Edition collaborative',
        text: 'Créez simplement des documents collaboratifs en ligne en accès partagé. Importez-les ensuite dans différents formats.',
        program: 'Etherpad'
      },
      {
        icon: 'webcam',
        title: 'Visio-conférences',
        text: 'S’éviter un long déplacement pour une courte réunion, mais pouvoir se voir, partager des documents en ligne...',
        program: 'Jitsi'
      },
      {
        icon: 'card-checklist',
        title: 'Sondages',
        text: 'Trouver une date de réunion, prendre une décision collective, tout simplement, en ligne.',
        program: 'Sondage'
      },
      {
        icon: 'people',
        title: 'Gestion centralisée',
        text: 'La gestion de comptes utilisateurs, des droits et des mots de passe se trouve dans une interface unique',
        program: 'Zourit-admin'
      }
    ]
    services_facultatifs = [
      {
        icon: 'graph-up',
        title: 'Comptabilité, gestion des adhésions',
        text: "Votre gestion comptable accessible en ligne à tout moment. La gestion de vos adhérents simplifiée.",
        price: 0,
        bool_name: 'garradin',
        program: 'Garradin'
      },
      {
        icon: 'send',
        title: 'Liste de diffusion',
        text: "Le service Sympa permet d'automatiser les fonctions de gestion des listes telles que les abonnements, la modération et la gestion des archives",
        price: 20,
        bool_name: 'sympa',
        program: 'Sympa'
      },
      {
        icon: 'cloud-arrow-up',
        title: 'Cloud dédié',
        text: "Un cloud dédié, vous permet de partager des documents entre membres de plusieurs structures (utile par exemple pour les Fédérations)",
        price: 200,
        bool_name: 'dedicated_cloud',
        program: 'DedicatedCloud'
      }
    ]

    services.each do |service|
      Service.create icon: service[:icon], title: service[:title], text: service[:text], mandatory: true, program: service[:program]
    end

    services_facultatifs.each do |service|
      Service.create icon: service[:icon], title: service[:title], text: service[:text], mandatory: false, program: service[:program], annual_price: service[:price]
    end

  end
end
