class CreateInvoices < ActiveRecord::Migration[7.0]
  def change
    create_table :invoices do |t|
      t.references :history, null: false, foreign_key: true
      t.float :price
      t.integer :days
      t.boolean :invoiced
      t.boolean :invoiced_provider

      t.timestamps
    end
  end
end
