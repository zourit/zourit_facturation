class ChangeAdresseToText < ActiveRecord::Migration[7.0]
  def change
    change_column :organizations, :address, :text
  end
end
