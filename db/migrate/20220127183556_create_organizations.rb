class CreateOrganizations < ActiveRecord::Migration[7.0]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :address
      t.string :zip_code
      t.string :town
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :domain
      t.string :first_account
      t.text :comment

      t.timestamps
    end
  end
end
