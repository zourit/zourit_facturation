module FormsHelper

  def calcul_price(form)
    price = form.quota * 1.4
    price += 20 if form.sympa
    price += 200 if form.dedicated_cloud
    return price
  end

end
