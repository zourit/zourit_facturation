class Invoice < ApplicationRecord
  belongs_to :history
  after_create_commit { broadcast_append_later_to('invoices', invoice: self) }

  def price_provider
    price = self.history.price_diff_or_price(1)
    days = self.history.remaining_days.to_f
    (days*price/365).round(2)
  end
end
