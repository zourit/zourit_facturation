class Form < ApplicationRecord

    def first_account_complete
        return "#{self.first_account}@#{self.domain}"
    end

    def contact
        return "#{self.first_name} #{self.last_name}"
    end
end
