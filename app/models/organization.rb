class Organization < ApplicationRecord
  has_many :histories, :dependent => :delete_all
  has_many :invoices, through: :histories
  accepts_nested_attributes_for :histories, allow_destroy: true

  def actual_services
    self.histories.last&.services
  end

  def actual_services_ids
    self.histories.last&.array_services_ids
  end

  def first_account_complete
    "#{self.first_account}@#{self.domain}"
  end

  def contact
    "#{self.first_name} #{self.last_name}"
  end

  def actual_quota
    self.histories.last&.quota
  end

  def annual_price
    self.histories.last&.annual_price
  end

  def start_date
    self.histories.first&.date
  end

  #def invoices
  #  self.histories.map(&:invoice).compact
  #end

end
