class History < ApplicationRecord
  belongs_to :organization
  has_one :invoice, :dependent => :delete

  def services
    self.services_ids&.split(',')&.map { |e| Service.find(e.to_i) } || []
  end

  def services_to_s
    services_line = ""
    services.each do |service|
      services_line << "#{service.program}, "
    end
    services_line[0,services_line.length-2]
  end

  def array_services_ids
    self.services_ids&.split(',')&.map { |e| e.to_i } || []
  end

  def annual_price(pu=1.4)
    price = self.quota ? (self.quota*pu).round(2) : 0
    self.services&.each do |service|
      price += service.annual_price
    end
    return price
  end

  def next
    self.class.unscoped.where("date >= ? AND id != ? AND organization_id=?", date, id, self.organization_id).order("date ASC").first
  end

  def previous
    self.class.unscoped.where("date <= ? AND id != ? AND organization_id=?", date, id, self.organization_id).order("date DESC").first
  end

  def price_diff(pu=1.4)
    previous_history = self.previous
    previous_history ? (self.annual_price(pu) - previous_history.annual_price(pu)).round(2) : 0
  end

  def price_diff_or_price(pu=1.4)
    price = self.annual_price(pu)
    price = price_diff if self.previous
    return price
  end

  def annual_price_with_diff(pu=1.4)
    diff = self.price_diff
    price = self.annual_price(pu).to_s
    if diff > 0
      price += " (+#{diff})"
    elsif diff < 0
      price += " (#{diff})"
    end
    price
  end

  def quota_diff
    previous_history = self.previous
    previous_history ? (self.quota - previous_history.quota) : 0
  end

  def quota_with_diff
    diff = quota_diff
    quota_string = self.quota.to_s
    if diff > 0
      quota_string += " (+#{diff})"
    elsif diff < 0
      quota_string += " (#{diff})"
    end
    quota_string
  end

  def remaining_days
    date_fin = Date.parse "#{self.date.year}-12-31"
    (date_fin-self.date).to_i
  end

end
