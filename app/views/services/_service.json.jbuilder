json.extract! service, :id, :title, :icon, :text, :annual_price, :mandatory, :program, :created_at, :updated_at
json.url service_url(service, format: :json)
