json.extract! invoice, :id, :history_id, :price, :days, :invoiced, :invoiced_provider, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
