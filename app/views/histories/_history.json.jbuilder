json.extract! history, :id, :organization_id, :quota, :services, :comment, :created_at, :updated_at
json.url history_url(history, format: :json)
