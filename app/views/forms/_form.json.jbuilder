json.extract! form, :id, :start_date, :name, :address, :zip_code, :town, :first_name, :last_name, :email, :phone, :domain, :first_account, :quota, :garradin, :sympa, :dedicated_cloud, :community_price, :comments, :created_at, :updated_at
json.url form_url(form, format: :json)
