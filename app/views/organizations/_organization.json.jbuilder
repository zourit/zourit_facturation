json.extract! organization, :id, :name, :address, :zip_code, :town, :first_name, :last_name, :email, :phone, :domain, :first_account, :created_at, :updated_at
json.url organization_url(organization, format: :json)
