class OrganizationsController < ApplicationController
  before_action :set_organization, only: %i[ show edit update destroy ]
  before_action :set_services, only: %i[ show edit new]

  # GET /organizations or /organizations.json
  def index
    @organizations = Organization.all
  end

  # GET /organizations/1 or /organizations/1.json
  def show
    @services_facultatifs = Service.where(mandatory: false)
  end

  # GET /organizations/new
  def new
    @organization = Organization.new
    @organization.histories.build    
  end

  # GET /organizations/1/edit
  def edit
  end

  # POST /organizations or /organizations.json
  def create
    @organization = Organization.new(organization_params)

    respond_to do |format|
      if @organization.save
        history = @organization.histories.last
        days = history.remaining_days
        Invoice.create history_id: history.id,
                       days: days,
                       price: (days * history.annual_price / 365).round(2) 
        notices = [{title: "Création effectuée", 
                   message: "Votre structure est correctement enregistrée"}]                                                  
        format.html { redirect_to organization_url(@organization), notice: notices }
        format.json { render :show, status: :created, location: @organization }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1 or /organizations/1.json
  def update
    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to organization_url(@organization), notice: "Organization was successfully updated." }
        format.json { render :show, status: :ok, location: @organization }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1 or /organizations/1.json
  def destroy
    @organization.destroy

    respond_to do |format|
      format.html { redirect_to organizations_url, notice: "Organization was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
    end

    def set_services
      @services_inclus = Service.where(mandatory: true)
      @services_facultatifs = Service.where(mandatory: false)
      @min_date = Date.today+1
      @end_date = Date.parse("31/12/#{@min_date.year}")
      @nb_jours = (@end_date - @min_date).to_i
    end

    # Only allow a list of trusted parameters through.
    def organization_params
      params.require(:organization)
            .permit(:name, :address, :zip_code, :town, :first_name, :last_name, :email, :phone, :domain, :first_account,
                    histories_attributes: [:quota, :services_ids, :date, :comment])
    end
end
