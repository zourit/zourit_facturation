class FormsController < ApplicationController
  include FormsHelper

  before_action :set_form, only: %i[ show edit update destroy ]
  before_action :init_params, only: %i[ new edit update structure ]

  # GET /forms or /forms.json
  def index
    @forms = Form.all
  end

  # GET /forms/1 or /forms/1.json
  def show
  end

  # GET /forms/new
  def new
    @form = Form.new
    @form.quota = 50
  end

  # GET /forms/1/edit
  def edit
  end

  # POST /forms or /forms.json
  def create
    @form = Form.new(form_params)
    @form.annual_price = calcul_price(@form)
    @form.community_price = true
    respond_to do |format|
      if @form.save
        format.html { redirect_to forms_url, notice: "Form was successfully created." }
        format.json { render :show, status: :created, location: @form }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @form.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forms/1 or /forms/1.json
  def update
    respond_to do |format|
      if @form.update(form_params) 
        @form.annual_price = calcul_price(@form)
        p calcul_price(@form)
        @form.save
        format.html { redirect_to forms_url, notice: "Form was successfully updated." }
        format.json { render :show, status: :ok, location: @form }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @form.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forms/1 or /forms/1.json
  def destroy
    @form.destroy

    respond_to do |format|
      format.html { redirect_to forms_url, notice: "Form was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # GET /structures
  def structures
    @structures = Form.all.order(:name)
  end

  # GET /structures/1
  def structure
    @structure = Form.find(params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form
      @form = Form.find(params[:id])
    end

    def init_params
      @services = [
        {
          icon: 'envelope',
          title: 'Courriel, agendas, contacts',
          text: 'Consultez vos mails, agendas partagés, contacts. Prenez rendez-vous ou tchattez en direct grâce à Zimbra.',
          program: 'Zimbra'
        },
        {
          icon: 'cloud',
          title: 'Fichiers dans le cloud',
          text: 'Grace au service Nextcloud, stockez vos fichiers en ligne afin de les retrouver à tout moment. Partagez-les simplement avec vos collègues.',
          program: 'Nextcloud'
        },
        {
          icon: 'pencil',
          title: 'Edition collaborative',
          text: 'Créez simplement des documents collaboratifs en ligne en accès partagé. Importez-les ensuite dans différents formats.',
          program: 'Etherpad'
        },
        {
          icon: 'webcam',
          title: 'Visio-conférences',
          text: 'S’éviter un long déplacement pour une courte réunion, mais pouvoir se voir, partager des documents en ligne...',
          program: 'Jitsi'
        },
        {
          icon: 'card-checklist',
          title: 'Sondages',
          text: 'Trouver une date de réunion, prendre une décision collective, tout simplement, en ligne.',
          program: 'Sondage'
        },
        {
          icon: 'people',
          title: 'Gestion centralisée',
          text: 'La gestion de comptes utilisateurs, des droits et des mots de passe se trouve dans une interface unique',
          program: 'Gestion centralisée'
        }
      ]
      @services_facultatifs = [
        {
          icon: 'graph-up',
          title: 'Comptabilité, gestion des adhésions',
          text: "Votre gestion comptable accessible en ligne à tout moment. La gestion de vos adhérents simplifiée.",
          price: 'Service compris dans le forfait',
          bool_name: 'garradin',
          program: 'Garradin'
        },
        {
          icon: 'send',
          title: 'Liste de diffusion',
          text: "Le service Sympa permet d'automatiser les fonctions de gestion des listes telles que les abonnements, la modération et la gestion des archives",
          price: '+20€ par an',
          bool_name: 'sympa',
          program: 'Sympa'
        },
        {
          icon: 'cloud-arrow-up',
          title: 'Cloud dédié',
          text: "Un cloud dédié, vous permet de partager des documents entre membres de plusieurs structures (utile par exemple pour les Fédérations)",
          price: '+200€ par an',
          bool_name: 'dedicated_cloud',
          program: 'Cloud dédié'
        }
      ]
      @min_date = Date.today+1
      @end_date = Date.parse("31/12/#{@min_date.year}")
      @nb_jours = (@end_date - @min_date).to_i
    end

    # Only allow a list of trusted parameters through.
    def form_params
      params.require(:form).permit(:start_date, :name, :address, :zip_code, :town, :first_name, :last_name, :email, :phone, :domain, :first_account, :quota, :garradin, :sympa, :dedicated_cloud, :community_price, :comments, :cgu)
    end
end
