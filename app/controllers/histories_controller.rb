class HistoriesController < ApplicationController
  before_action :set_history, only: %i[ show edit update destroy ]
  before_action :set_organization


  # GET /histories or /histories.json
  def index
    @histories = @organization.histories
  end

  # GET /histories/1 or /histories/1.json
  def show
  end

  # GET /histories/new
  def new
    prev = @organization.histories.last
    @history = History.new
    @history.organization_id = @organization.id
    @history.date = @organization.histories.last.date + 1
    @default_quota = prev ? prev.quota : 50
    @default_services = prev ? prev.services_ids : ''
  end

  # GET /histories/1/edit
  def edit
  end

  # POST /histories or /histories.json
  def create
    prev = @organization.histories.last
    @history = History.new(history_params)
    @history.organization_id = @organization.id

    changes = false
    changes = true if @history.quota != prev.quota
    changes = true if @history.services_ids != prev.services_ids

      if changes
        if @history.save
          notice = [{title: "Création effectuée", 
                     message: "L'avenant est correctement enregistré"}]
          price = @history.price_diff_or_price
          if price > 0
            invoice = Invoice.new
            invoice.history_id = @history.id
            invoice.price = price
            invoice.days = @history.remaining_days
            if invoice.save
              redirect_to organization_histories_path, notice: notice.push(title: "Facture créée", 
                                                                           message: "La facture liée à cette avenant a bien été créée")
            else
              redirect_to organization_url(@organization), notice: notice.push(title: "Echec de la création de la facture", 
                                                                               message: "La facture liée à cette avenant n'a pas pu être créée",
                                                                               warning: true)
            end
          else
            redirect_to organization_path(@organization), notice: notice
          end
        else
          render :new, status: :unprocessable_entity
        end
      else
        redirect_to organization_url(@organization), notice: [{title: "Aucun changement détecté", 
                                                               message: "L'avenant n'a pas été créé", 
                                                               warning: true}]
      end
  end

  # PATCH/PUT /histories/1 or /histories/1.json
  def update
    respond_to do |format|
      if @history.update(history_params)
        format.html { redirect_to histories_url, notice: "History was successfully updated." }
        format.json { render :show, status: :ok, location: @history }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /histories/1 or /histories/1.json
  def destroy
    @history.destroy

    respond_to do |format|
      format.html { redirect_to histories_url, notice: "History was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_history
      @history = History.find(params[:id])
    end

    def set_organization
      @organization = Organization.find(params[:organization_id])
    end

    # Only allow a list of trusted parameters through.
    def history_params
      params.require(:history).permit(:organization_id, :quota, :services_ids, :comment, :date)
    end
end
